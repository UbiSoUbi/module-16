// HomeWprk_16.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <time.h>

using namespace std;

int main()
{

#pragma endregion
#pragma region NxN array declared and showed in console

    const int N = 5; // Rank of arrray
    int array[N][N]; // declare array
    int IndexSum = 0; // declare sum of i-elements
#pragma region get data time
#pragma warning(disable : 4996)
    time_t t = time(NULL); //  holding the number of seconds (not counting leap seconds) since 00:00, Jan 1 1970
    struct tm tm = *localtime(&t);  //Converts given time since epoch(a time_t value pointed to by timer) into calendar time, 
    //expressed in local time, in the struct tm format.The result is stored in static storageand a pointer to that static storage is returned.

    //printf("Year: %d\n", tm.tm_year + 1900);
    //printf("Month: %d\n", tm.tm_mon + 1);
    //printf("Day: %d\n", tm.tm_mday);
    //printf("Hour: %d\n", tm.tm_hour);
    //printf("Minute: %d\n", tm.tm_min);
    //printf("Second: %d\n", tm.tm_sec);
    //printf("\n");
    int Left = tm.tm_mday % N ; // Left of Days in month after divided by N(rank of array)
    cout << "Left of Days/N(rank of array): "<< Left;
    cout << " \n\n";
#pragma endregion
    cout << "Array:\n";
    for (size_t i = 0; i < N; i++) // a[i][] from 0 to N
    {
        for (size_t j = 0; j < N; j++) //a[i][j] from 0 to N
        {
            array[i][j] = i + j;  
            cout << array[i][j];
            if (i == Left)  // get the sum of all elements in searching index
            {
                IndexSum += array[i][j];
            }
        }
        cout << " \n";
    }
    cout << "\nSum of i = Day%N\n";
    cout << IndexSum; 
#pragma endregion



}

